'''
Created on 8 sept. 2013

@author: efarhan
'''

import pygame
from pygame.locals import *

UP,DOWN,LEFT,RIGHT, = 0,0,0,0
UP2,DOWN2,LEFT2,RIGHT2 = 0,0,0,0
ACTION,END,RETRY = 0,0,0
axis = ()
joystick = 0
index = 0
mouse_click,mouse_pos = 0,(0,0)
def init():
    global joystick
    if pygame.joystick.get_count() != 0:
        joystick = pygame.joystick.Joystick(0)
        joystick.init()
def loop():
    global joystick,index,axis,UP,DOWN,LEFT,RIGHT,UP2, DOWN2,LEFT2,RIGHT2,ACTION,END,RETRY,mouse_click,mouse_pos
    # check events (with joystick)
    for event in pygame.event.get(): 
        if (joystick != 0):
            
            if (event.type == JOYHATMOTION):
                if (joystick.get_hat(0) == (0, 1)):
                    UP = 1
                elif(joystick.get_hat(0) == (0, -1)):
                    # DOWN
                    pass
                elif(joystick.get_hat(0) == (1, 0)):
                    RIGHT = 1
                elif(joystick.get_hat(0) == (-1, 0)):
                    # LEFT
                    LEFT = 1
                elif(joystick.get_hat(0) == (0, 0)):
                    UP, RIGHT,LEFT = 0, 0,0
            elif event.type == JOYAXISMOTION:
                if(joystick.get_axis(0)>0.9):
                    RIGHT = 1
                else:
                    RIGHT = 0
                if(joystick.get_axis(0)<-0.9):
                    LEFT = 1
                else:
                    LEFT = 0
                #get_axis
                axis=[joystick.get_axis(i) for i in range(joystick.get_numaxes())]
            elif event.type == JOYBUTTONDOWN:
                if(joystick.get_button(1)):
                    UP = 1
                if(joystick.get_button(4)):
                    index -= 1
                if(joystick.get_button(5)):
                    index += 1
            elif event.type == JOYBUTTONUP:
                if(not joystick.get_button(1)):
                    UP = 0
        if event.type == MOUSEBUTTONUP:
            if event.button == 1:
                mouse_click = False
        if event.type == MOUSEBUTTONDOWN:
            if event.button == 1:
                mouse_click = True
        if event.type == KEYDOWN:
            if event.key == K_UP:
                UP2 = 1
            elif event.key == K_w:
                UP = 1
            elif event.key == K_DOWN:
                DOWN2 = 1 
            elif event.key == K_s:
                    # DOWN
                DOWN = 1
            elif event.key == K_RIGHT :
                RIGHT2 = 1
            elif event.key == K_d:
                RIGHT = 1
            elif event.key == K_LEFT :
                LEFT2 = 1
            elif event.key == K_a:
                    # LEFT
                LEFT = 1
            elif event.key == K_ESCAPE:
                END = 1
            elif event.key == K_r:
                RETRY = 1
        if event.type == KEYUP:
            if event.key == K_UP :
                UP2=0
            elif event.key == K_w:
                UP = 0
            elif event.key == K_DOWN :
                DOWN2 = 0
            elif event.key == K_s:
                    # DOWN
                DOWN = 0
            elif event.key == K_RIGHT :
                RIGHT2 = 0
            elif event.key == K_d:
                RIGHT = 0
            elif event.key == K_LEFT :
                LEFT2 = 0
            elif event.key == K_a:
                    # LEFT
                LEFT = 0
            elif event.key == K_ESCAPE:
                END = 0  
            elif event.key == K_r:
                RETRY = 0        
        if event.type == QUIT:
                END = 1
        mouse_pos = pygame.mouse.get_pos()
def get_index():
    global index
    return index
def is_end():
    global END
    return END
def get_keys():
    global RIGHT,LEFT,UP,DOWN,ACTION
    return (RIGHT,LEFT,UP,DOWN,ACTION)
def get_secondary_keys():
    global RIGHT2,LEFT2,UP2,DOWN2
    return (RIGHT2,LEFT2,UP2,DOWN2)
def get_retry():
    global RETRY
    return RETRY
def get_axis():
    global axis
    return axis
def get_mouse():
    global mouse_click, mouse_pos
    return mouse_click, mouse_pos