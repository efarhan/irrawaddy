'''
Created on Sep 5, 2013

@author: efarhan
'''

import os
from os import listdir
from os.path import isfile, join
from engine.const import animation_step, debug
from engine.event import get_keys, get_secondary_keys
from Box2D import *
from physics.contact_listener import PlatformerContactListener
from physics.physics import pixel2meter, meter2pixel
from engine.const import jump_step
from engine.const import invulnerability
import engine
from engine.image_manager import img_manager
from animation import Animation
import pygame

class PlatformerAnimation(Animation):
    '''Manage the images and the animation of the demo-player'''
    def __init__(self,size):
        Animation.__init__(self, size)
        self.end_climb = False
        self.img = 0
        self.anim_counter = 0
        self.size = size
        self.foot_num = 0
        self.jump = 1
        self.right_side = True
        self.physics = None
        self.move = 2
        self.climb = 0
        self.foot_sensor_size = (20,0.1)
        self.already_jumped = False
        self.jumped = False
        self.jump_step = 0
        self.down_ground, self.wall, self.up_ground = False,False,False
        self.UP, self.RIGHT,self.LEFT,self.DOWN,self.ACTION = 0, 0, 0, 0, 0
        self.path = "data/sprites/perso/"
        self.img_indexes = []
        self.anim_speed = 2
        self.state = ""
        self.on_climb = False
        self.path_list = ["still_right","still_left",
            "move_right", "move_left",
            "climb_right","climb_left",
            "climb_end_right",
            "climb_end_left"]
        self.state_range = {
            "still_right":(0,32),"still_left":(32,63),
            "move_right":(64,84), "move_left":(84,104),
            "climb_right":(104,128),"climb_left":(128,152),
            "climb_end_right":(152,183),
            "climb_end_left":(183,214)
            }
        self.load_images()
        
        
        
    def loop(self,player):
        #check event
        
        (self.RIGHT, self.LEFT,self.UP,self.DOWN,self.ACTION) = get_keys()
        second_keys = get_secondary_keys()
        (self.RIGHT,self.LEFT,self.UP,self.DOWN) = (self.RIGHT or second_keys[0],
                                                    self.LEFT or second_keys[1],
                                                    self.UP or second_keys[2],
                                                    self.DOWN or second_keys[3])
        
        if(self.climb and (self.UP or self.DOWN)):
            self.move = 0
        else:
            self.move = 2
        
            
        
        if self.RIGHT and not self.LEFT:
            #animation
            if (self.move == 1 or self.move == 2) and (self.climb == 0 or self.foot_num != 0):
                
                self.set_state('move_right')
                self.right_side = True
                #move the player
            self.physics.move(player,1)
                
        if self.LEFT and not self.RIGHT:
                #move the player
            if (self.move == -1 or self.move == 2) and (self.foot_num != 0 or self.climb == 0):
                self.set_state('move_left')
                self.right_side = False
            self.physics.move(player,-1)
        if self.UP and not self.DOWN:
            #climb
            if (self.climb == 1 or self.climb == 2) and not self.on_climb:
                if self.right_side:
                    if(self.end_climb):
                        self.set_state('climb_end_right')
                    else:
                        self.set_state('climb_right')
                else:
                    self.set_state('climb_left')
                self.physics.move(player,None,-1)
            
        if self.DOWN and not self.UP:
            #climb down
            if (self.climb == -1 or self.climb == 2) and not self.on_climb:
                if self.right_side:
                    if(self.end_climb):
                        self.set_state('climb_end_right', invert=True)
                    else:
                        self.set_state('climb_right',invert=True)
                else:
                    self.set_state('climb_left',invert=True)
                self.physics.move(player,None,1)
                
        if self.climb and not self.UP and not self.DOWN:
            if self.foot_num == 0:
                self.physics.move(player,0,0)
        if not self.RIGHT and not self.LEFT:
            if self.foot_num >= 1 and not self.climb:
                if self.right_side:
                    self.set_state('still_right')
                else:
                    self.set_state('still_left')
                #stop the player
            self.physics.move(player,0)
        if self.RIGHT and self.LEFT:
            self.physics.move(player,0)
    def init_physics(self,player):
        self.physics = player.level.physics
        self.body = self.physics.add_dynamic_object(player)
        self.box = self.body.CreatePolygonFixture(box = (pixel2meter(player.box_size[0]), pixel2meter(player.box_size[1])), density=1,friction=0)
        
        self.box_rect = pygame.Rect((player.pos[0]-player.box_size[0],player.pos[1]-player.box_size[1]),(2*player.box_size[0],2*player.box_size[1]))
        self.box.density = 0
        self.box.userData = 5
        self.body.fixedRotation = True
        self.body.angle = 0
        #add foot sensor
        polygon_shape = b2PolygonShape()
        polygon_shape.SetAsBox(pixel2meter(self.foot_sensor_size[0]), self.foot_sensor_size[1]+1, 
                               b2Vec2(0,pixel2meter(player.box_size[1])),0)
        
        fixture_def = b2FixtureDef()
        fixture_def.shape = polygon_shape
        fixture_def.density = 0
        fixture_def.isSensor = True
        self.foot_sensor_fixture = self.body.CreateFixture(fixture_def)
        self.foot_sensor_fixture.userData = 3
        self.foot_rect = pygame.Rect((player.pos[0]-self.foot_sensor_size[0],player.pos[1]+player.box_size[1]),
                                     (self.foot_sensor_size[0]*2,int(meter2pixel(self.foot_sensor_size[1]*2))))
        #add end climb sensor
        fixture_def,polygon_shape = None,None
        polygon_shape = b2PolygonShape()
        polygon_shape.SetAsBox(pixel2meter(30), self.foot_sensor_size[1]+1,
                               b2Vec2(0,pixel2meter(-90)),0)
        
        fixture_def = b2FixtureDef()
        fixture_def.shape = polygon_shape
        fixture_def.density = 0
        fixture_def.isSensor = True
        self.end_climb_sensor_fixture = self.body.CreateFixture(fixture_def)
        self.end_climb_sensor_fixture.userData = 4
        self.end_climb_rect = pygame.Rect((player.pos[0]-self.foot_sensor_size[0],player.pos[1]+player.box_size[1]),
                                     (self.foot_sensor_size[0]*2,int(meter2pixel(self.foot_sensor_size[1]*2))))
        self.contact_listener = PlatformerContactListener()
        self.physics.world.contactListener = self.contact_listener
        
