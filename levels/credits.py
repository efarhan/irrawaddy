from engine.scene import Scene
from engine.init import get_screen_size
from game_object.text import Text
import pygame
from engine.const import framerate
from engine.sound_manager import snd_manager
#font_obj, msg, sound_obj

class Credits(Scene):
  def init(self):
    self.credits = pygame.image.load('data/sprites/credits/credits.png').convert()
    self.offset_y = 0
    self.frames = 0
    self.credits_alpha = 255
    snd_manager.play_music("data/music/credits.ogg")
    
  def loop(self, screen):
    if (self.credits_alpha == 0):
        from engine.level_manager import switch
        switch(0)
    #ofs = -(self.offset_y % 720)
    #screen.blit(self.credits[self.offset_y / 720], (0,ofs))
    #screen.blit(self.credits[(self.offset_y + 720) / 720], (0,720+ofs))
    screen.blit(self.credits, (0,0), (0, self.offset_y, 1280, 720))
    self.frames += 1
    if (self.frames % 2 == 0 and self.offset_y < 4400):
        self.offset_y += 1
    elif (self.frames > 9920 - 510): # Music - Fadeout
        self.credits.set_alpha(int(self.credits_alpha))
        self.credits_alpha -= 0.5