'''
Created on 9 dec. 2013

@author: efarhan
'''
import pygame
from engine.init import get_screen_size
from engine.scene import Scene
from engine.const import framerate

from game_object.player import Player

from physics.physics import Physics
from animation.platformer_animation import PlatformerAnimation
from game_object.ground import Ground

from game_object.image import Image
from game_object.mask import Mask
from engine.event import get_mouse
from game_object.speech import Speech

class GamePlay(Scene):
    def __init__(self):
        Scene.__init__(self)
    def init(self):
        self.intro_promise_speech= Speech(["I can't go back.\nI made a promise."])
        self.intro_promise_speech.pos = (500,300)
        self.intro_promise_touch = False
        self.fadein, self.fadeout = True, False
        self.alpha = 255
        self.speed = int(3*framerate/60.0)
        
        self.black_screen = pygame.Surface(get_screen_size(), flags=pygame.SRCALPHA)
        self.black_screen.fill(pygame.Color(0,0,0,0))
        
        self.screen_pos=(0,0)
        
        self.physics = Physics()
        self.physics.init()
        self.player = Player(self)
        self.player.pos = (get_screen_size()[0]/3,get_screen_size()[1]*2/3)
        self.player.set_animation(PlatformerAnimation(self.player.size))
        #self.cliff = Cliff(self.physics, (get_screen_size()[0]*2/3-25,get_screen_size()[1]*2/3-self.player.size[1]/2),(get_screen_size()[0]*2/3-25,0),left=True)
        self.hidden_messages = []
        Scene.init(self)
    def loop(self, screen):
        for m in self.hidden_messages:
            m.loop(screen,self.screen_pos,self.mask)
        
        self.player.loop(screen, self.screen_pos)
        
        self.physics.loop()
        #print init_pos_player,self.player.pos 

        self.screen_pos = (int(self.player.pos[0])-get_screen_size()[0]/3,int(self.player.pos[1])-(get_screen_size()[1]*2/3-self.player.size[1]/2))
        if(self.fadein and self.alpha > 0):
            self.alpha -= self.speed
            if(self.alpha <= 0):
                self.alpha = 0
                self.fadein = False
            
        if(self.fadeout and self.alpha < 255):
            self.alpha += self.speed
            if(self.alpha >= 255):
                self.fadeout = False
                self.alpha = 255
        self.black_screen.fill(pygame.Color(0,0,0,self.alpha))
        screen.blit(self.black_screen,(0,0))
        Scene.loop(self, screen)
        if self.intro_promise_touch:
            self.intro_promise_speech.loop(screen)
        else:
            self.intro_promise_speech.reset()