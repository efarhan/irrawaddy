'''
Created on 15 dec. 2013

@author: efarhan
'''
from engine.scene import Scene
from levels.gameplay import GamePlay
import pygame
from engine.init import get_screen_size
from engine.scene import Scene
from engine.const import framerate

from game_object.player import Player

from physics.physics import Physics
from animation.platformer_animation import PlatformerAnimation
from game_object.mask import Mask
from levels.gameplay import GamePlay
from game_object.image import Image
from game_object.grass_ground import GrassGround
from game_object.ground import Ground
from engine.event import get_mouse
from game_object.physics_object import AngleSquare
from game_object.speech import Speech
from game_object.hidden_message import HiddenMessage
from game_object.masked_image import MaskedImage
from game_object.divided_image import DividedImage

class Forest(GamePlay):
    def __init__(self):
        GamePlay.__init__(self)
    def init(self):
        GamePlay.init(self)
        self.speech = Speech(["I met someone like you",
                              "I think I love you",
                              "Deborah did not know",
                              "Me neither..." 
                              ])
        self.end = False
        self.mask = Mask("light", (0,0), get_screen_size(),delta_pos=(470-615, 284-360))
        self.background = DividedImage("data/sprites/forest/fond-foret-etendu.png",(0,0))
        #self.cliff = Cliff(self.physics, (get_screen_size()[0]*2/3-25,get_screen_size()[1]*2/3-self.player.size[1]/2),(get_screen_size()[0]*2/3-25,0),left=True)
        self.arriere_plan = MaskedImage("data/sprites/forest/arriere-plan-foret-etendu-grimpe-silhouette-sol.png", 
                                        "data/sprites/forest/arriere-plan-foret-etendu-grimpe-silhouette-sol-light.png", (0,0))
        self.arriere_plan_haut = MaskedImage("data/sprites/forest/arriere-plan-foret-etendu-grimpe-silhouette-haut.png", 
                                        "data/sprites/forest/arriere-plan-foret-etendu-grimpe-silhouette-haut-light.png", (0,-360+1))
        self.ground = DividedImage("data/sprites/forest/sol-herbeforet-etendu.png",(0,0))
        #self.tree = pygame.image.load("data/sprites/forest/arbre-grimper-silhouette-etendu.png").convert_alpha()
        self.color_tree = DividedImage("data/sprites/forest/arbre-grimper-etendu.png",(0,0))
        self.physics_objects = [
                    AngleSquare((320,0),(50,get_screen_size()[1]),self.physics,0,2),
                    AngleSquare((0,630), (self.background.size[0]*2+200,720-630), self.physics, 0, 0, False),
                    AngleSquare((650,100),(50,get_screen_size()[1]),self.physics,0,-1,True),
                    AngleSquare((650+self.background.size[0],100),(50,get_screen_size()[1]),self.physics,0,-1,True)
                    ]
        self.premier_plan = DividedImage("data/sprites/forest/premier-plan-foret-etendu-grimpe.png",(0,0))
        self.hidden_messages = [HiddenMessage("data/sprites/text/05-I made a Mistake.png",(476+236,168-135))]
        self.hidden_speech = Speech(["We used to climb together..."])
        self.hidden_speech_touch = False
        self.hidden_speech.pos = (400,250)
    def loop(self, screen):
        self.player.step_sounds = self.player.step_grass
        if (self.player.pos[0]>2*self.background.size[0]-get_screen_size()[0]/2):
            self.end = True
            self.fadeout = True
        if(self.end):
            if self.alpha == 255:
                from engine.level_manager import switch
                switch("bridge")#bridge
        if get_mouse()[0]:
            print get_mouse()[1],self.screen_pos
        screen.fill((0,0,0))
    
        #self.ground2.loop(screen, self.screen_pos)
        if(self.player.pos[0]>self.background.size[0]-get_screen_size()[0]):
            self.background.loop(screen, self.screen_pos, (self.background.size[0],-get_screen_size()[1]/2))
        if(self.player.pos[0]<self.background.size[0]+get_screen_size()[0]):
            self.background.loop(screen, self.screen_pos,(0,-get_screen_size()[1]/2))
        
        self.mask.loop()
        self.arriere_plan.loop(screen, self.screen_pos, self.mask)
        self.arriere_plan_haut.loop(screen, self.screen_pos, self.mask)
        self.arriere_plan.loop(screen,self.screen_pos,self.mask,(self.background.size[0],0))
        self.arriere_plan_haut.loop(screen, self.screen_pos, self.mask,(self.background.size[0],-360))
        
        if(self.player.pos[0]<self.background.size[0]+get_screen_size()[0]):
            self.color_tree.loop(screen, self.screen_pos,(0,-get_screen_size()[1]/2))
        if(self.player.pos[0]>self.background.size[0]-get_screen_size()[0]):
            self.color_tree.loop(screen, self.screen_pos,(self.background.size[0],-get_screen_size()[1]/2))
        
        if(self.player.pos[0]<self.background.size[0]+get_screen_size()[0]):
            self.ground.loop(screen, self.screen_pos, (0,-get_screen_size()[1]/2))
        if(self.player.pos[0]>self.background.size[0]-get_screen_size()[0]):
            self.ground.loop(screen, self.screen_pos, (self.background.size[0],-get_screen_size()[1]/2))
        GamePlay.loop(self, screen)
        if(self.player.pos[0]<self.background.size[0]+get_screen_size()[0]):
            self.premier_plan.loop(screen, self.screen_pos, (0,-get_screen_size()[1]/2))
        if(self.player.pos[0]>self.background.size[0]-get_screen_size()[0]):
            self.premier_plan.loop(screen, self.screen_pos,(self.background.size[0],-get_screen_size()[1]/2))
        self.speech.loop(screen)
        if(self.player.pos[0]>self.background.size[0] and self.player.pos[1]<267-153):
            self.hidden_speech_touch = True
        if self.hidden_speech_touch:
            self.hidden_speech.loop(screen)
        for o in self.physics_objects:
            o.loop(screen, self.screen_pos)