'''
Created on 15 dec. 2013

@author: efarhan
'''
import pygame
from engine.init import get_screen_size
from engine.scene import Scene
from engine.const import framerate

from game_object.player import Player

from physics.physics import Physics
from animation.platformer_animation import PlatformerAnimation
from game_object.mask import Mask
from levels.gameplay import GamePlay
from game_object.image import Image
from game_object.grass_ground import GrassGround
from game_object.ground import Ground
from engine.event import get_mouse
from engine.scene import Scene
from levels.gameplay import GamePlay
from game_object.physics_object import AngleSquare
class CliffLevel(GamePlay):
    def __init__(self):
        GamePlay.__init__(self)
    def init(self):
        GamePlay.init(self)
        #self.mask = Mask("light", (0,0), get_screen_size(),delta_pos=(470-615, 260-360))
        self.background = pygame.image.load("data/sprites/cliff/cliff-fondciel.png").convert_alpha()
        #self.cliff = Cliff(self.physics, (get_screen_size()[0]*2/3-25,get_screen_size()[1]*2/3-self.player.size[1]/2),(get_screen_size()[0]*2/3-25,0),left=True)
        #self.barrier = pygame.image.load("data/sprites/cliff/cliff-arriere-plan.png").convert_alpha()
        self.ground = pygame.image.load("data/sprites/cliff/cliff-arriere-plan.png").convert_alpha()
        pos1 = (490+804,462+149)
        pos2 = (1060+3146,64+149)      
        
        delta_pos = ((1060+3146)-(490+804),(64+149)-(462+149))
        self.physics_objects = [
                AngleSquare((0,630), (5122,200), self.physics, 0),
                AngleSquare((pos1[0],pos1[1]+delta_pos[1]/2+20),(delta_pos[0],50),self.physics,8)]
        self.hide_square = pygame.Surface((1942+1224,150))
        self.hide_square.fill(pygame.Color(0,0,0))           
        #self.ground2 = Ground((self.player.pos[0]-200,self.player.pos[1]+self.player.size[1]/2), (100,1), self.physics)
    def loop(self, screen):
        if (self.player.pos[0]>self.background.get_size()[0]):
            from engine.level_manager import switch
            switch(0)
        if get_mouse()[0]:
            print get_mouse()[1],self.screen_pos
        screen.fill((0,0,0))
        screen.blit(self.background, (-2000+int(-self.screen_pos[0]*0.1),0))
        screen.blit(self.hide_square,(-self.screen_pos[0],get_screen_size()[1]-self.hide_square.get_size()[1]))
        #self.ground2.loop(screen, self.screen_pos)
        
        for o in self.physics_objects:
            o.loop(screen, self.screen_pos)
        GamePlay.loop(self, screen)
        screen.blit(self.ground,(-self.screen_pos[0],-self.screen_pos[1]))