'''
Created on 11 sept. 2013

@author: efarhan
'''
import pygame
from engine.scene import Scene
from game_object.speech import Speech
from engine.init import get_screen_size

class TheEnd(Scene):
    def init(self):
        Scene.init(self)
        self.speech = Speech(["Finally..."])
        self.speech.pos = (get_screen_size()[0]/2,get_screen_size()[1]/2)

    def loop(self, screen):
        self.speech.loop(screen)
        if self.speech.alpha == -1:
            from engine.level_manager import switch
            switch("credits")