'''
Created on 14 dec. 2013

@author: efarhan
'''
import pygame
from engine.init import get_screen_size
from engine.scene import Scene
from engine.const import framerate

from game_object.player import Player

from physics.physics import Physics
from animation.platformer_animation import PlatformerAnimation
from game_object.mask import Mask
from levels.gameplay import GamePlay
from game_object.image import Image
from game_object.grass_ground import GrassGround
from game_object.ground import Ground
from engine.event import get_mouse
from game_object.physics_object import AngleSquare
from game_object.speech import Speech
from game_object.hidden_message import HiddenMessage
from game_object.masked_image import MaskedImage
from game_object.divided_image import DividedImage
class Campagne(GamePlay):
    def __init__(self):
        GamePlay.__init__(self)
        self.end = False
    def init(self):
        self.ambiance = pygame.mixer.Sound("data/sound/atmosphere/outside.ogg")
        self.ambiance.play(loops=-1)
        GamePlay.init(self)
        self.speech = Speech(["I remembered the day of our wedding.", 
                              "I was smiling to everyone", 
                              "I kissed Deborah",
                              "You looked at us and smiled"])
        self.mask = Mask("light", (0,0), get_screen_size(),delta_pos=(470-615, 260-360))
        self.background = DividedImage("data/sprites/countryside/countryside-extended-background.png",(0,0))
        #self.cliff = Cliff(self.physics, (get_screen_size()[0]*2/3-25,get_screen_size()[1]*2/3-self.player.size[1]/2),(get_screen_size()[0]*2/3-25,0),left=True)
        self.barrier = MaskedImage("data/sprites/countryside/countryside-extended-mp-shadow.png","data/sprites/countryside/countryside-extended-mp-light-v2.png",(0,0))
        self.ground = DividedImage("data/sprites/countryside/countryside-extended-ground.png",(0,0))
        self.hill = MaskedImage("data/sprites/countryside/countryside-hill.png","data/sprites/countryside/countryside-hill-light.png",(2560,0))
        self.tunnel = DividedImage("data/sprites/countryside/tunnel-countryside-sortie.png",(0,0))
        self.tree = DividedImage("data/sprites/countryside/countryside-end-transition-foret.png",(2560,0))
        pos_1 = (295+2460,128+149)
        pos_2 = (1113+3750,477+149)
        delta_pos = (pos_2[0]-pos_1[0],pos_2[1]-pos_1[1])
        self.physics_objects = [
                    AngleSquare((320,0),(50,get_screen_size()[1]),self.physics,0,2),
                    AngleSquare((0,630), (2560*2,720-630), self.physics, 0, 0, False),
                    AngleSquare((580+2148,149+130+23),(50,470-130),self.physics,0,1,False),
                    AngleSquare((pos_1[0],pos_1[1]+delta_pos[1]/2+14),(delta_pos[0],50),self.physics,-9)]
        self.hide_down = pygame.Surface((get_screen_size()[0],200))
        self.hide_down.fill(pygame.Color(0,0,0))
        
        self.hidden_messages = [
                    HiddenMessage("data/sprites/text/03-I was happy.png", (948+351,322+149-20)),
                    HiddenMessage("data/sprites/text/04-It always comes back to you.png", (751+2453,290-170))]
    def loop(self, screen):
        
        self.player.step_sounds = self.player.step_grass
        if (self.player.pos[0]>self.background.size[0]+self.hill.size[0]-get_screen_size()[0]*3/4):
            self.end = True
            self.fadeout = True
        if(self.end):
            if self.alpha == 255:
                self.ambiance.stop()
                from engine.level_manager import switch
                switch("forest")
        if get_mouse()[0]:
            print get_mouse()[1],self.screen_pos
        screen.fill((0,0,0))
        self.mask.loop()
        self.background.loop(screen, (int(self.screen_pos[0]*0.1),int(0.1*(self.screen_pos[1]))), (0,-30) )
        screen.blit(self.hide_down, (0,get_screen_size()[1]-self.screen_pos[1]))
        #self.ground2.loop(screen, self.screen_pos)
        if (self.player.pos[0]>self.background.size[0]-get_screen_size()[0]*3/4):
            self.hill.loop(screen, self.screen_pos, self.mask)
            self.tree.loop(screen, self.screen_pos)
        if (self.player.pos[0]<self.background.size[0]+get_screen_size()[0]*3/4):
            self.barrier.loop(screen, self.screen_pos, self.mask)
            self.tunnel.loop(screen,self.screen_pos)
        self.ground.loop(screen, self.screen_pos)
        GamePlay.loop(self, screen)
        self.speech.loop(screen)
        for o in self.physics_objects:
            o.loop(screen, self.screen_pos)
        