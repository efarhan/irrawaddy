'''
Created on 15 dec. 2013

@author: efarhan
'''
from engine.scene import Scene
from levels.gameplay import GamePlay
import pygame
from engine.init import get_screen_size
from engine.scene import Scene
from engine.const import framerate

from game_object.player import Player

from physics.physics import Physics
from animation.platformer_animation import PlatformerAnimation
from game_object.mask import Mask
from levels.gameplay import GamePlay
from game_object.image import Image
from game_object.grass_ground import GrassGround
from game_object.ground import Ground
from engine.event import get_mouse
from game_object.speech import Speech
from game_object.physics_object import AngleSquare
from engine.sound_manager import snd_manager
from game_object.hidden_message import HiddenMessage
from game_object.masked_image import MaskedImage

class Tunnel(GamePlay):
    def __init__(self):
        GamePlay.__init__(self)
    def init(self):
        self.end_reach = False
        self.ambiance = pygame.mixer.Sound("data/sound/atmosphere/sewers.ogg")
        self.ambiance.play(loops=-1)
        GamePlay.init(self)
        snd_manager.play_music("data/music/main_theme.ogg")
        self.mask = Mask("light", (0,0), get_screen_size(),delta_pos=(470-615, 260-360))
        self.speech = Speech(["We made a promise to each other", 
                              "Be on that bridge the ninth of august at midnight", 
                              "It was ten years ago...",
                              "It is the last thing that\nkeeps me alive"])

        self.color_barrier = MaskedImage(None,"data/sprites/tunnel/tunnel-light.png",(0,0))
        self.end = MaskedImage("data/sprites/tunnel/tunnel-fin-egout-shadow-court.png","data/sprites/tunnel/tunnel-fin-egout-court-lumiere.png",(2*2560,0))
        self.physics_objects = [
                    AngleSquare((320,0),(50,get_screen_size()[1]),self.physics,0,2),
                    AngleSquare((0,630), (2560*3,720-630), self.physics, 0, 0, False),
                    ]
        self.hidden_messages = [
                    HiddenMessage("data/sprites/text/01- you just wanted to talk.png", (get_screen_size()[0]+200,get_screen_size()[1]/2)),
                    HiddenMessage("data/sprites/text/02-I wanted to kiss you.png", (3*get_screen_size()[0]+210,get_screen_size()[1]/2))]
    def loop(self, screen):
        
        self.player.step_sounds = self.player.step_reverb
        if (self.player.pos[0]>2560*2+get_screen_size()[0]):
            
            self.end_reach = True
            self.fadeout = True
        if(self.end_reach and self.alpha == 255):
            self.ambiance.stop()
            from engine.level_manager import switch
            switch("campagne")
        if get_mouse()[0]:
            print get_mouse()[1],(get_screen_size()[0]/2,get_screen_size()[1]/2)
        screen.fill((0,0,0))
        #self.ground2.loop(screen, self.screen_pos)
        
        self.mask.loop()
        self.mask.pos = self.screen_pos
        if(self.player.pos[0]<2560+get_screen_size()[0]):
            self.color_barrier.loop(screen, self.screen_pos, self.mask)
            
        if(self.player.pos[0]>2560-get_screen_size()[0]):
            self.color_barrier.loop(screen, self.screen_pos, self.mask, (2560,0))
        if(self.player.pos[0]>2*2560-get_screen_size()[0]):
            self.end.loop(screen, self.screen_pos, self.mask)
        GamePlay.loop(self, screen)

        self.speech.loop(screen)