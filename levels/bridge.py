'''
Created on 15 dec. 2013

@author: efarhan
'''
from engine.scene import Scene
from levels.gameplay import GamePlay
import pygame
from engine.init import get_screen_size
from engine.scene import Scene
from engine.const import framerate

from game_object.player import Player

from physics.physics import Physics
from animation.platformer_animation import PlatformerAnimation
from game_object.mask import Mask
from levels.gameplay import GamePlay
from game_object.image import Image
from game_object.grass_ground import GrassGround
from game_object.ground import Ground
from engine.event import get_mouse
from game_object.physics_object import AngleSquare
from engine.sound_manager import snd_manager
from game_object.speech import Speech
from game_object.hidden_message import HiddenMessage
from game_object.masked_image import MaskedImage
class BridgeLevel(GamePlay):
    def __init__(self):
        GamePlay.__init__(self)
    def init(self):
        GamePlay.init(self)
        self.time1, self.time2 = 10, 20
        self.speech1 = Speech(["Why is it always happening to me?", "I did not deserve it"])
        self.speech1.pos = (500,250)
        self.speech2 = Speech(["I should have kissed you\nback then...","I feel so alone"])
        self.speech2.pos = (self.speech2.pos[0],400)
        snd_manager.play_music("data/music/the_end_is_near.ogg")
        self.end = False
        self.mask = Mask("light", (0,0), get_screen_size(),delta_pos=(470-615, 260-360))
        self.background = pygame.image.load("data/sprites/bridge/Bridge-cliff-background.png").convert_alpha()
        self.background = pygame.transform.scale(self.background,(4376,1000)).convert_alpha()
        self.color_barrier = pygame.image.load("data/sprites/bridge/Bridge-cliff-mp-lights.png").convert_alpha()
        self.ground = pygame.image.load("data/sprites/bridge/Bridge-cliff-mp-shadows.png").convert_alpha()
        delta = (1300,0)
        a_pos1 = (delta[0]+315+286,delta[1]+286+39)
        a_pos2 = (delta[0]+967+286,delta[1]+321+39)
        a_delta_pos = (a_pos2[0]-a_pos1[0],a_pos2[1]-a_pos1[1])
        b_pos1 = (delta[0]+1119+115,delta[1]+321+38)
        b_pos2 = (delta[0]+1119+382,delta[1]+321+38)
        b_delta_pos = (b_pos2[0]-b_pos1[0],0)
        c_pos1 = (delta[0]+535+965,delta[1]+483-122)
        c_pos2 = (delta[0]+937+965,delta[1]+643-122)
        c_delta_pos = (c_pos2[0]-c_pos1[0],c_pos2[1]-c_pos1[1])
        
        delta_cliff = 3000
        pos1 = (delta_cliff+490+804,462+149)
        pos2 = (1059+7052,(72+39))
        delta_pos = (pos2[0]-pos1[0],pos2[1]-pos1[1])
        self.tree = MaskedImage("data/sprites/bridge/tree1-shadow.png", "data/sprites/bridge/tree1-shadow.png", (1402,-100))
        self.tree2 = MaskedImage("data/sprites/bridge/tree2-shadow.png", "data/sprites/bridge/tree2-shadow.png", (3561,-100))
        self.physics_objects = [
                AngleSquare((333,0),(50,get_screen_size()[1]),self.physics,0,2),
                AngleSquare((0,520), (9000,200), self.physics, 0,0),
                AngleSquare((0,630), (2560*2,720-630), self.physics, 0, 0, False),
                AngleSquare((a_pos1[0],a_pos1[1]+a_delta_pos[1]/2+100),(a_delta_pos[0],50),self.physics,15),
                AngleSquare((b_pos1[0],b_pos1[1]),(b_delta_pos[0],50),self.physics,0),
                AngleSquare((c_pos1[0]-17,c_pos1[1]+c_delta_pos[1]/2+3),(c_delta_pos[0]+265,50),self.physics,-15),
                AngleSquare((pos1[0],pos1[1]+delta_pos[1]/2+20),(delta_pos[0],50),self.physics,8),
                AngleSquare((1600,300),(50,300),self.physics,0,-1,True),
                AngleSquare((3780,300),(50,300),self.physics,0,-1,True),
                                ]
        self.dialog = False
        self.speech = Speech(["Where are you?", "Do not tell me\nyou will not come!", "Should I continue my way?"])
        self.hidden_messages = [HiddenMessage("data/sprites/text/07-the end is ahead.png",(419+1423,215+39)),
                                HiddenMessage("data/sprites/text/wait for me.png",(163+1205,89-180)),
                                HiddenMessage("data/sprites/text/I waited for you.png",(3700,89-180))]
        self.speech_you_timing = 15
        self.halo_pos_timing = 0
        self.halo_pos = (0,0)
        self.halo = pygame.image.load("data/sprites/bridge/halo.png").convert_alpha()
        self.counter = 0
        self.good_end_speech = Speech(["I'm coming"])
        self.good_end_speech.pos = (0,get_screen_size()[1]/2)
    def loop(self, screen):
        
        if(387+1348<self.player.pos[0]<389+3100):
            self.player.step_sounds = self.player.step_normal
        else:
            self.player.step_sounds = self.player.step_grass
        if(self.player.pos[0]>2238):
            self.dialog = True

        if (self.player.pos[0]>7294+404):
            self.end = True
            self.fadeout = True
            snd_manager.fadeout_music(0)
        if(self.end):
            if self.alpha == 255:
                from engine.level_manager import switch
                switch("TheEnd")
        if get_mouse()[0]:
            print get_mouse()[1],self.screen_pos
        screen.fill((0,0,0))
        screen.blit(self.background,(-int(self.screen_pos[0]*0.1),-self.screen_pos[1]/2-200))
        self.mask.loop()
        self.tree.loop(screen, self.screen_pos, self.mask)
        self.tree2.loop(screen, self.screen_pos, self.mask)
        GamePlay.loop(self, screen)
        if self.dialog:
            screen.blit(self.halo,(self.halo_pos[0]-self.screen_pos[0],self.halo_pos[1]))
        screen.blit(self.ground, (-self.screen_pos[0],-self.screen_pos[1]-100))
        
        self.mask.pos = self.screen_pos
        masked = self.color_barrier.copy()
        masked.blit(self.mask.get_mask(),self.mask.pos,None, pygame.BLEND_RGBA_MULT)
        screen.blit(masked,(-self.screen_pos[0],-self.screen_pos[1]-100))
        if self.dialog:
            self.counter+=1
            if(self.counter < 2*60*60):
                self.halo_pos = (2120*(self.counter/(2*60*60.0)),0)
            if(self.player.pos[0]<self.halo_pos[0]+700):
                self.end = True
                self.fadeout = True
            if self.counter > framerate*30 and self.player.pos[0]<2750:
                self.good_end_speech.loop(screen)
            self.speech.loop(screen)
            if(self.time1 <= 0):
                self.speech1.loop(screen)
            else:
                self.time1 -=1
            if(self.time2 <= 0):
                self.speech2.loop(screen)
            else:
                self.time2 -=1
             