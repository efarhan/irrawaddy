'''
Created on 15 dec. 2013

@author: efarhan
'''
from levels.gameplay import GamePlay
from game_object.physics_object import AngleSquare
from engine.init import get_screen_size
import pygame
from engine.event import get_mouse
from game_object.masked_image import MaskedImage
from game_object.mask import Mask
from game_object.divided_image import DividedImage
class GameplayTest(GamePlay):


    def __init__(self):
        GamePlay.__init__(self)
        
    def init(self):
        GamePlay.init(self)
        self.test_img = MaskedImage("data/sprites/FOREST/arriere-plan-foret-etendu-grimpe-silhouette-sol.png","data/sprites/FOREST/arriere-plan-foret-etendu-grimpe-silhouette-sol-light.png", (0,239))
        self.physics_objects = [ 
                        AngleSquare((-self.test_img.size[0]/2,get_screen_size()[1]), (2*self.test_img.size[0],100),self.physics, 0),]

        self.mask = Mask("light", (0,0), get_screen_size(),delta_pos=(470-615, 260-360))
    def loop(self, screen):
        if get_mouse()[0]:  
            print get_mouse()[1],(get_screen_size()[0]/2,get_screen_size()[1]/2)
        screen.fill(pygame.Color(255,255,255))
        self.test_img.loop(screen, self.screen_pos,self.mask)

        for o in self.physics_objects:
            o.loop(screen,self.screen_pos)
        
        GamePlay.loop(self, screen)