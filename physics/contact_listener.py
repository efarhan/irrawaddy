'''
Created on Sep 9, 2013

@author: efarhan
'''
import engine
from Box2D import *

class PlatformerContactListener(b2ContactListener):
    def BeginContact(self, contact):
        fixture_user_data = contact.fixtureA.userData
        fixture_user_data2 = contact.fixtureB.userData
        # feet is touching something
        player = engine.level_manager.level.player
        print "Begin Contact", fixture_user_data, fixture_user_data2, player.anim.on_climb
        if((fixture_user_data == 3 and (fixture_user_data2 != 1 and fixture_user_data2 != -1))\
           or (fixture_user_data2 == 3 and (fixture_user_data != 1 and fixture_user_data != -1))):
            player.anim.foot_num += 1
        if(fixture_user_data == 3 and fixture_user_data2 == 1)\
           or (fixture_user_data2 == 3 and fixture_user_data == 1):
            player.anim.on_climb = True
        if(fixture_user_data == 5 and (fixture_user_data2 == 1 or fixture_user_data2 == -1)) or ((fixture_user_data == 1 or fixture_user_data == -1) and fixture_user_data2 == 5):
            player.anim.climb = 2
            player.anim.end_climb = False
        if(fixture_user_data == 5 and fixture_user_data2 == 2) or (fixture_user_data == 2 and fixture_user_data2 == 5):
            engine.level_manager.level.intro_promise_touch = True
        
    def EndContact(self, contact):
        fixture_user_data = contact.fixtureA.userData
        fixture_user_data2 = contact.fixtureB.userData
        player = engine.level_manager.level.player
        print "End Contact", fixture_user_data, fixture_user_data2, player.anim.on_climb
        if((fixture_user_data == 3  and (fixture_user_data2 != 1 and fixture_user_data2 != -1)) or (fixture_user_data2 == 3 and (fixture_user_data != 1 and fixture_user_data != -1))):
            # feet is touching something
            player.anim.foot_num -= 1
        if(fixture_user_data == 3 and fixture_user_data2 == 1)\
           or (fixture_user_data2 == 3 and fixture_user_data == 1):
            player.anim.on_climb = False
        if(fixture_user_data == 5 and (fixture_user_data2 == 1 or fixture_user_data2 == -1)) or ((fixture_user_data == 1 or fixture_user_data == -1) and fixture_user_data2 == 5):
            player.anim.climb = 0
            player.anim.end_climb = False
        if(fixture_user_data == 5 and fixture_user_data2 == 2) or (fixture_user_data == 2 and fixture_user_data2 == 5):
            engine.level_manager.level.intro_promise_touch = False
        if((fixture_user_data == 4 and fixture_user_data2 == 1) or (fixture_user_data == 1 and fixture_user_data2 == 2)):
            player.anim.end_climb = True