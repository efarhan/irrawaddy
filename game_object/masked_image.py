'''
Created on 16 dec. 2013

@author: efarhan
'''
from image import Image
import pygame
from engine.init import get_screen_size, screen_size
from engine.image_manager import img_manager
from divided_image import DividedImage

class MaskedImage(DividedImage):
    def __init__(self, path, masked_path, pos):
        self.masked_path = masked_path
        self.masked_imgs = []
        DividedImage.__init__(self, path, pos)
    def load_images(self):
        DividedImage.load_images(self)
        self.masked_img_index = img_manager.load(self.masked_path)
        if self.path == None:
            self.size = img_manager.images[self.masked_img_index].get_size()
        self.divide_images(img_manager.images[self.masked_img_index], self.masked_imgs)
    def loop(self, screen, screen_pos,mask,new_pos=(0,0)):
        DividedImage.loop(self, screen, screen_pos, new_pos)
        i = 0
        for row in self.masked_imgs:
            j = 0
            for img in row:
                pos = (0,0)
                if new_pos == (0,0):
                    pos = self.pos
                else:
                    pos = new_pos
                pos = (pos[0]+(i)*self.cuted_size[0],pos[1]+(j)*self.cuted_size[1])
                
                rect = pygame.Rect((-pos[0]+screen_pos[0],-pos[1]+screen_pos[1]),get_screen_size())
                if rect.colliderect(img.get_rect()):
                    #show img
                    mask.pos = (-pos[0]+screen_pos[0],-pos[1]+screen_pos[1])
                    masked = img.copy()
                    masked.blit(mask.get_mask(),mask.pos,None, pygame.BLEND_RGBA_MULT)
                    screen.blit(masked,(pos[0]-screen_pos[0],pos[1]-screen_pos[1]))
                j+=1
            i+=1