'''
Created on 14 dec. 2013

@author: efarhan
'''
import pygame
from image import Image
class GrassGround(Image):
    def __init__(self, path, pos, size,physics, height, factor=1,):
        Image.__init__(self, path, pos, size, factor=factor)
        self.physics = physics
        self.height = height
        self.init_physics()
    def loop(self, screen, screen_pos, new_size=1):
        Image.loop(self, screen, screen_pos, new_size=new_size)
    def load_images(self):
        Image.load_images(self)
    def init_physics(self):
        print "POS: ", (self.pos[0], self.pos[1]+self.height),(self.size[0],self.size[1]-self.height)
        self.physics.add_static_box((self.pos[0], self.pos[1]+self.size[1]/2-(self.size[1]-self.height)/2),(self.size[0],self.size[1]-self.height))