'''
Created on 14 dec. 2013

@author: efarhan
'''
import pygame
from image import Image
from engine.const import animation_step
from engine.image_manager import img_manager
from engine.init import get_screen_size
already_load = False
class Mask(Image):
    def __init__(self, path, pos, size, delta_pos,factor=1):
        self.delta_pos = delta_pos
        Image.__init__(self, path, pos, size, factor=factor)
        self.anim_speed = 6
    def load_images(self):
        global already_load
        Image.load_images(self)
        if not already_load:
            for img in self.images:
                alpha_sw = pygame.Surface(get_screen_size(), flags=pygame.SRCALPHA)
                alpha_sw.fill(pygame.Color(0,0,0,0))
                alpha_sw.blit(img_manager.images[img],self.delta_pos,None,pygame.BLEND_RGBA_ADD)
                img_manager.images[img] = alpha_sw
            already_load = True
    def loop(self):
        if(self.anim_counter == self.anim_speed):
            self.anim_counter = 0
            if(self.img_index == self.img_number-1):
                self.img_index = 0
            else:
                self.img_index += 1
        else:
            self.anim_counter +=1
    def get_mask(self):
        return img_manager.images[self.images[self.img_index]]