'''
Created on 24 sept. 2013

@author: tenchi@team2xh.net
'''
import pygame
from random import randint
from game_object import GameObject

class Speech(GameObject):
    def reset(self):
        self.counter = 0
        self.textcount = 0
        self.fadecount = 0
        self.next_glitch = 0
        self.alpha = 255
        self.text_buf.fill((0,0,0))
    # text must be a list of strings ("sentences" below)
    def __init__(self,text):
        # Timing variables
        self.counter       = 0 # Frame counter
        self.textcount     = 0 # Character counter
        self.sentencecount = 0 # Sentence counter
        self.fadecount     = 0 # Number of character not drawn before fade-out
        self.next_glitch   = randint(20, 120) # Time (in frames) before next glitch
        # Speech settings
        self.pos   = (300,100)
        self.speed = 10 # How many frames for each character
        self.text  = [s.upper() for s in text]
        # Fontmap
        self.font_width  = 20
        self.font_height = 40
        self.xspacing    = 16
        self.yspacing    = 38
        self.buf_size = (30 * self.font_width, 2 * self.font_height) #
        self.fontmap = pygame.image.load('data/sprites/text/dialog_font.png').convert_alpha()
        self.fontmap_glitched = pygame.image.load('data/sprites/text/dialog_font_glitched.png').convert_alpha()
        self.used_fontmap = self.fontmap
        self.mapdict = {'A':(0,0), 'G':(6,0), 'M':(12,0),'S':(5,1), 'Y':(11,1),'(':(4,2), '-':(10,2),
                        'B':(1,0), 'H':(7,0), 'N':(0,1), 'T':(6,1), 'Z':(12,1),')':(5,2),
                        'C':(2,0), 'I':(8,0), 'O':(1,1), 'U':(7,1), '.':(0,2), ':':(6,2),
                        'D':(3,0), 'J':(9,0), 'P':(2,1), 'V':(8,1), ',':(1,2), ';':(7,2),
                        'E':(4,0), 'K':(10,0),'Q':(3,1), 'W':(9,1), '"':(2,2), '!':(8,2),
                        'F':(5,0), 'L':(11,0),'R':(4,1), 'X':(10,1),"'":(3,2), '?':(9,2) 
                        }
        # Buffers
        self.trns_buf = pygame.Surface(self.buf_size).convert() # buffer for transparency
        self.text_buf = pygame.Surface(self.buf_size).convert() # buffer for text
        self.alpha = 255
                        
    # Returns the rect of the character in the fontmap
    def letter(self, c):
        pos = self.mapdict.get(c, (12,2))
        return pygame.Rect(pos[0]*self.font_width, pos[1]*self.font_height,
                              self.font_width - 1, self.font_height - 1)
    
    def loop(self, screen):
        self.counter += 1
        # OPTIMIZATION: If we have nothing to draw or it's too soon, do nothing
        if (self.alpha <= 0 or self.counter < 60):
            return
        
        # Fake transparency hack by blitting what's on the screen at the same pos
        self.trns_buf.blit(screen, (-self.pos[0], -self.pos[1],
                           self.buf_size[0], self.buf_size[1])) 
        
        # Each 10 frames:
        if (self.counter % self.speed == 0):
            # If we are at a glitch time, set the next glitch and swap the font
            if (self.counter > self.next_glitch):
                self.used_fontmap = self.fontmap_glitched
                self.next_glitch += randint(20, 120)
            # Else put back the normal font
            else:
                self.used_fontmap = self.fontmap
            # Increment the character counter
            if (self.textcount < len(self.text[self.sentencecount])):
                self.textcount += 1
            # If we finished printing the characters, start the pre-fade-out timer
            else:
                self.fadecount += 1
                
            # Variables for newline formatting
            line = 0
            line_i = 0
            # Clear text trns_buf
            self.text_buf.fill((0,0,0))
            # For each letter till our current character
            for i in xrange(self.textcount):
                if (self.text[self.sentencecount][i] == '\n'):
                    line += 1
                    line_i = i+1
                else:
                    # Random jittering + character position
                    letterpos = (i*16 + randint(0,1) - line*line_i*self.xspacing,
                                 line*self.yspacing + randint(0,1))
                    self.text_buf.blit(self.used_fontmap, letterpos, self.letter(self.text[self.sentencecount][i]))

        # We start fading out after the time of printing 6 characters is over past the last character drawn
        if (self.fadecount > 6 and self.alpha >= 0):
            self.alpha -= 2
        # If we finished our current sentence, go to next one
        if (self.alpha <= 0 and self.sentencecount < len(self.text)-1):
            self.reset()
            self.sentencecount += 1
        
        # Blit the text trns_buf over the transparency trns_buf and then to the screen
        self.trns_buf.blit(self.text_buf, (0,0), (0,0,self.buf_size[0],self.buf_size[1]), pygame.BLEND_ADD)
        self.trns_buf.set_alpha(self.alpha)
        screen.blit(self.trns_buf, self.pos)
        
    # Give new text to the object
    def set_newtext(self, text):
        self.text = text
        self.reset()
        self.sentencecount = 0
