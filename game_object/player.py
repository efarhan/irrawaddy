'''
Created on 8 sept. 2013

@author: efarhan
'''
import pygame


from game_object import GameObject

from physics.physics import pixel2meter



class Player(GameObject):
    def __init__(self,level):
        GameObject.__init__(self,level.physics)
        self.level = level
        self.font = pygame.font.Font('data/font/8-BITWONDER.ttf',25)
        self.anim = None
        self.size = (300,300)
        self.box_size = (25, 150)
        self.step_counter = 0
        self.step_grass  = [pygame.mixer.Sound("data/sound/sfx/footsteps_grass/"+str(i+1)+".ogg") for i in xrange(4)]
        self.step_normal = [pygame.mixer.Sound("data/sound/sfx/footsteps_hard/"+str(i+1)+".ogg") for i in xrange(4)]
        self.step_reverb = [pygame.mixer.Sound("data/sound/sfx/footsteps_hard_reverb/"+str(i+1)+".ogg") for i in xrange(4)]
        self.step_sounds = self.step_normal
        
    def init_animation(self,anim):
        self.anim = anim
        self.anim.load_images()
        self.anim.physics = self.level.physics
        self.anim.init_physics(self)
    def set_animation(self,anim):
        
        self.anim = anim
        self.anim.physics = self.level.physics
        self.anim.load_images()
        self.anim.init_physics(self)


    def loop(self, screen,screen_pos,new_size=1):
        if self.anim != None:
            self.anim.loop(self)
        
        # show the current img
        self.pos = (int(self.pos[0]), int(self.pos[1]))
        #pygame.draw.rect(screen,pygame.Color(255,0,0),self.anim.box_rect)
        if ((self.anim.state == "move_right" or self.anim.state == "move_left") and self.anim.anim_counter == self.anim.anim_speed):
            find_index = self.anim.img_indexes.index(self.anim.img)
            if (find_index == 84+8 or find_index == 84+18 or
                find_index == 64+8 or find_index == 64+18):
                self.step_sounds[self.step_counter].play()
                self.step_counter = (self.step_counter + 1) % 4
        #pygame.draw.rect(screen,pygame.Color(255,0,0),self.anim.foot_rect)
        self.img_manager.show(self.anim.img, screen, (self.pos[0]-screen_pos[0],self.pos[1]-screen_pos[1]),factor=new_size)
        return self.pos
    
    def set_position(self,new_pos):
        self.pos = new_pos
        if(self.anim != None):
            self.anim.body.position = (pixel2meter(new_pos[0]),pixel2meter(new_pos[1]))
        