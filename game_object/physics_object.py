'''
Created on 15 dec. 2013

@author: efarhan
'''
import pygame
import math
from engine.image_manager import rot_center
from physics.physics import pixel2meter
from Box2D.Box2D import b2Vec2
from engine.const import debug
class AngleSquare():
    def __init__(self,pos,size,physics,angle=0,data=0,sensor=False):
        self.data = data
        self.pos = pos
        self.size = size
        self.angle = angle
        self.rect = pygame.Rect(self.pos, self.size)
        self.surface = pygame.Surface(self.size,flags=pygame.SRCALPHA)
        self.surface.fill(pygame.Color(255,0,0,255))
        self.physics = physics
        self.sensor = sensor
        self.init_physics()
    def init_physics(self):
        print "Init Physics"
        self.rad_angle = math.radians(self.angle)
        center_pos = self.rect.center
        i = self.physics.add_static_box(center_pos,self.size,angle=-self.rad_angle,data=self.data,sensor=self.sensor)
        self.physics.static_objects[i].position = b2Vec2(pixel2meter(self.rect.center[0]),pixel2meter(self.rect.center[1]))
    def loop(self,screen,screen_pos):
        if debug:
            rot_image, rot_rect = rot_center(self.surface, self.rect, self.angle)
            screen.blit(rot_image, (rot_rect[0]-screen_pos[0],rot_rect[1]-screen_pos[1]))