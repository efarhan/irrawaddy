'''
Created on 16 dec. 2013

@author: efarhan
'''
from image import Image
from engine.image_manager import img_manager
import pygame
from engine.init import get_screen_size
class HiddenMessage(Image):


    def __init__(self, path, pos):
        Image.__init__(self, path, pos,None)
    def load_images(self):
        Image.load_images(self)
    def loop(self, screen, screen_pos, mask):
        #TODO optimize
        mask.pos = (screen_pos[0]-self.pos[0],screen_pos[1]-self.pos[1])
        masked = img_manager.images[self.images[0]].copy()
        masked.blit(mask.get_mask(),mask.pos,None, pygame.BLEND_RGBA_MULT)
        screen.blit(masked,(self.pos[0]-screen_pos[0],self.pos[1]-screen_pos[1]))
        